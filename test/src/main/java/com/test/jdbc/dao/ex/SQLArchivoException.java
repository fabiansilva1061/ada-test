package com.test.jdbc.dao.ex;

public class SQLArchivoException extends SQLException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6195588326250025157L;

	public SQLArchivoException(String error) {
		super(error);
	}

	public SQLArchivoException(String error, Throwable causa) {
		super(error, causa);
	}


}
