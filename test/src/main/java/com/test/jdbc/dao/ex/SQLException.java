package com.test.jdbc.dao.ex;

import org.springframework.dao.DataAccessException;

public class SQLException extends DataAccessException {

	private static final long serialVersionUID = -4443216637524710816L;

	public SQLException(String error) {
		super(error);
	}

	public SQLException(String error, Throwable causa) {
		super(error, causa);
	}

}
