package com.test.jdbc.dao.util;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.jdbc.dao.ex.SQLArchivoException;

public class DaoUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(DaoUtil.class);

	/**
	 * Permite leer el archivo SQL.
	 * 
	 * @param ubicacion
	 * @return
	 * @throws Exception
	 */
	public static String leerSQL(String ubicacion) throws SQLArchivoException {

		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Cargando el archivo SQL {}", ubicacion);
			}

			return new String(Files.readAllBytes(Paths.get(ubicacion)));
		} catch (Exception e) {
			LOGGER.error("Error al leer el archivo SQL {}", ubicacion, e);
			throw new SQLArchivoException("Error al leer el archivo SQL {}", e);
		}
	}

}
