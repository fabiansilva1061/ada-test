package com.test.respository.impl;

import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.test.jdbc.dao.Dao;
import com.test.model.Usuario;
import com.test.repository.UsuarioRepository;

@Repository
public class UsuarioRepositoryImpl extends Dao implements UsuarioRepository {

	@Override
	public Usuario registrar(Usuario domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void actualizar(Usuario domain) {
		// TODO Auto-generated method stub

	}

	@Override
	public void eliminar(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Usuario> obtenerTodos() {
		try {
			return getNamedParameterJdbcTemplate().query("SELECT * FROM TB_RECAUDO_DETALLE", (RowMapper<Usuario>) (rs, rowNum) -> {
				Usuario usuario = new Usuario();
				usuario.setNombres(rs.getString("NOMBRES_APELLIDOS"));
				return usuario;
			});
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	protected String getSequenceName() {
		// TODO Auto-generated method stub
		return null;
	}

}
