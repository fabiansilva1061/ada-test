package com.test.model;

import java.io.Serializable;

public class Usuario implements Serializable{

	private static final long serialVersionUID = -6650521377334791695L;
	
	private String nombres;

	private String apellidos;
	
	private String email;
	
	public Usuario() {
		
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
