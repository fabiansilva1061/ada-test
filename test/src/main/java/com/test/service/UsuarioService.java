package com.test.service;

import java.util.List;

import com.test.model.Usuario;

public interface UsuarioService {

	List<Usuario> obtenerListaUsuario ();
	
}
