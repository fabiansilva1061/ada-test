package com.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.model.Usuario;
import com.test.repository.UsuarioRepository;
import com.test.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRespository;

	@Override
	public List<Usuario> obtenerListaUsuario() {
		try {
			return usuarioRespository.obtenerTodos();
		} catch (Exception e) {
			return null;
		}
	}

}
