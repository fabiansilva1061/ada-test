package com.test.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.test.model.Usuario;
import com.test.service.UsuarioService;

@RequestMapping(value = "/test/api")
@Controller
public class TestRestController {

	@Autowired
	private UsuarioService usuarioService;

	@PostMapping(value = "/usuario/signup")
	public ResponseEntity<Usuario> registrarUsuario(@RequestBody Usuario usuario) {
		
		return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
	}

	@GetMapping(value = "/usuario/list")
	public ResponseEntity<List<Usuario>> obtenerUsuario() {
		try {
			return new ResponseEntity<List<Usuario>>(usuarioService.obtenerListaUsuario(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
