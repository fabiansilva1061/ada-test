package com.test.repository;

import java.util.List;

public interface CrudRepository<T> {

	public T registrar(T domain);

	public void actualizar(T domain);

	public void eliminar(Long id);

	public List<T> obtenerTodos();

}
