package com.test.repository;

import com.test.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario> {

}
